const Discord = require('discord.js');
const UserController = require('../Controllers/users');
const ClasseController = require('../Controllers/classes');

module.exports.run = async(client, message, args) => {
    let user =  message.mentions.members.first().user;
    let classe =  message.mentions.roles.first();
    let allClasses = ClasseController.getAllClasses();
    let idClasse;
    
    allClasses.forEach(element => {
        idClasse = element.idDiscord === classe.id ? element.id : null
    });
    if (!idClasse) { return message.channel.send("Veuillez rentrer une classe")}
    UserController.setClasse(idClasse, user.id);
    message.channel.send('L\'utilisateur '+ user.username +' a été ajouté');
};

module.exports.help = {
    name: "ajouter_classe"
};