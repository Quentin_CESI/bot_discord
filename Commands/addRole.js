module.exports.run = (client, message, args) => {
    if (!args.join(' ')) { return message.channel.send('Vous n\'avez pas la spécifié un nom de classe !'); }

    let member = message.guild.member(message.author.id);
    let role =  message.mentions.roles.first();
    console.log(role);
    if (!role) { return message.channel.send('Ce role n\'existe pas !'); }
    // pas propre
    if (member._roles.find((r) => r === role.id)) { return message.channel.send('Vous avez déjà ce role !'); }
    //if (role.name.substring(0,12) !== "bot_classe") { return message.channel.send('Veuillez saisir une classe !'); }


    let classes = [];
    // Ici tu peux faire une condition qui va chercher dans toute les classes que tu as chargé leur nom et les ajouter au tableau pour qu'il les retire a l'utilisateur ensuite
    // member.guild.roles.forEach(element => {
        // Pour l'instant ca marche avec un match mais du coup faudra que tu change
        // element.name.match(/^bot_classe/) ? classes.push(element) : null;
    // });
    
    classes.forEach(element => {
        let classExist = member.roles.find((r) => r.id === element.id);
        classExist ? member.removeRole(element.id)
            .then(()=> message.channel.send('Vous n\'avez désormais plus le role ' + element.toString()))
            .catch(console.error) : null;
    })

    console.log('----Role----');
    console.log(role);

    member.addRole(role)
        .then(() => message.channel.send('Vous avez désormais le role ' + role.toString()))
        .catch(console.error);
};

module.exports.help = {
    name: 'classe'
};