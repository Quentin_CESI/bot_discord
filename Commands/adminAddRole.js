module.exports.run = (client, message, args) => {
    if (!args.join(' ')) { return message.channel.send('Vous n\'avez pas la spécifié un nom de classe !'); }
    //if (!message.guild.member(message.author.id).hasPermission('MANAGE_ROLES')) { return message.channel.send('Vous n\'avez pas la permission `gérer les roles` !'); }
    //if (!message.guild.member(client.user.id).hasPermission('MANAGE_ROLES')) { return message.channel.send('Je n\'ai pas la permission `gérer les roles` !'); }
    
    // déclaration des variables
    // todo : ajouter une vérification sur les roles de l'auteur du message
    let member = message.guild.member(message.author.id);
    let targetUser = message.mentions.members.first();
    let role = message.mentions.roles.first();
    // Vérification de base
    if (!role) { return message.channel.send('Ce role n\'existe pas !'); }
    if (targetUser.roles.has(role.id)) { return message.channel.send('L\'utilisateur a déjà ce role !'); }
    // if (role.name.substring(0,7) !== "bot_classe_") { return message.channel.send('Veuillez saisir une classe !'); }
    // Suppression des roles de classes si il en a d'autres
    let classes = [];
    targetUser.guild.roles.forEach(element => {
        element.name.match(/^classe_/) ? classes.push(element) : null;
    });
    classes.forEach(element => {
        let classExist = targetUser.roles.find((r) => r.id === element.id);
        classExist ? targetUser.removeRole(element.id)
            .then(()=> message.channel.send('L\'utilisateur n\'a désormais plus le role ' + element.toString()))
            .catch(console.error) : null;
    })
    // Ajout du role
    targetUser.addRole(role)
        .then(() => message.channel.send('L\'utilisateur a désormais le role ' + role.toString()))
        .catch(console.error);
};

module.exports.help = {
    name: 'user_classe'
};