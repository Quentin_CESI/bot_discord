// Ici il faudra load les cours selon l'uutilisateur ou la classe ou alors un seul cours via une requête
const Cours = require('../Controllers/cours');

module.exports.run = async(client, message, args) => {
    // Ici une requête ou alors tu récupère tous les cours en tu les trie ici
    Cours.getAllCours().then((value) => {
        let coursInfos = [];
        value.forEach(element => {
            coursInfos.push({
                name: 'Nom :',
                value:  element.name 
            },
            {
                name: 'Description :',
                value:  element.description 
            })
        });
        message.channel.send({
            embed: {
                color: 0xe43333,
                title: `Info des cours de  **${message.author.username}**`,
                fields: coursInfos,
                footer: {
                    text: `Informations sur les cours de l'utilisateur ${message.author.username}`
                }
            }
        });
    });
};

module.exports.help = {
    name: "mes_cours"
};