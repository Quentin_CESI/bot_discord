// Ici il faudra load les cours ou alors un seul cours via une requête
const Cours = require('../Controllers/cours');

module.exports.run = async(client, message, args) => {
    // Ici une requête ou alors un find dans un tableau comme je fais
    let targetCours = message.content.match(/^!cours (\w*)/)[1];
    Cours.getAllCours().then((value) => {
        let cours = value.find((c) => c.name === targetCours);
        if (!cours) { return message.channel.send('Veuillez ressaisir votre cours !'); }

        message.channel.send({
            embed: {
                color: 0xe43333,
                title: `Info du cours de  **${cours.name}**`,
                fields: [
                    {
                        name: 'Nom :',
                        value:  cours.name 
                    },
                    {
                        name: 'Description :  :',
                        value: cours.description
                    }
                ],
                footer: {
                    text: `Informations sur le cours ${cours.name}`
                }
            }
        });
    });
};

module.exports.help = {
    name: "cours"
};