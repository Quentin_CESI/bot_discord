const Discord = require('discord.js');
const ClasseController = require('../Controllers/classes');

module.exports.run = async(client, message, args) => {
    let classe =  message.mentions.roles.first();
    ClasseController.insertClasse(classe.name, classe.id);
    message.channel.send('La classe '+ classe.name +' a été ajouté');
};

module.exports.help = {
    name: "ajouter_classe"
};