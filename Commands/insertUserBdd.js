const Discord = require('discord.js');
const UserController = require('../Controllers/users');

module.exports.run = async(client, message, args) => {
    let user =  message.mentions.members.first().user;
    UserController.insertUser(user.username, user.id);
    message.channel.send('L\'utilisateur '+ user.username +' a été ajouté');
};

module.exports.help = {
    name: "ajouter_user"
};