const Discord = require('discord.js');
const moment = require('moment');

module.exports.run = (client, message, args) => {
    console.log(message.mentions);
    const classe = message.mentions.roles.first();
    if (!classe) { return message.channel.send('Veuillez mentionner une classe !'); }
    
    message.channel.send({
        embed: {
            color: 0xe43333,
            title: `Statistiques du la classe **${classe.name}**`,
            fields: [
                {
                    name: 'ID :',
                    value: classe.id 
                },
                {
                    name: 'Crée le :',
                    value: moment.utc(classe.createdAt).format("LL")
                }
            ],
            footer: {
                text: `Informations de le la classe ${classe.name}`
            }
        }
    });
    
};

module.exports.help = {
    name: 'info_classe'
};