var mysql = require('mysql');
var mySqlClient = mysql.createConnection({
    host: "localhost",
    user: "root",
    database: "mysql"
});

module.exports = {
  getAllCours: function () {
    var selectQuery = 'SELECT * FROM bot_cours';
    return new Promise((resolve, reject) => {
      mySqlClient.query(
      selectQuery,
        function select(error, results, fields) {
          if (error) {
            console.log(error);
            reject(error);
          }
          if (results) {
            if ( results.length > 0 )  {
              ret = results;
              resolve(results);
            } else {
              console.log("Pas de données");
            }
          }
        }
      );
    });
  }
};