const Discord = require('discord.js');
const prefix = '!';

module.exports = (client, message) => {
    if (message.author.bot) { return; }
    if (!message.channel.permissionsFor(client.user).has('SEND_MESSAGES')) { return; }
    if (!message.content.startsWith(prefix)) { return; }

    let args = message.content.slice(prefix.length).trim().split(/ +/g);
    let commande = args.shift();
    let cmd = client.commands.get(commande);
    if(!client.commands.find((c) => {return c.help.name === commande})) { return message.channel.send('Commande non reconnue'); }
    if (!cmd) { return; }
        cmd.run(client, message, args);
};