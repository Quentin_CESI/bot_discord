// Valeur de test a dégager
const classeTest = [
    {
        id: 99,
        name: "bot_classe_cm2"
    },
    {
        id: 1,
        name: "bot_classe_1"
    },
    {
        id: 2,
        name: "bot_classe_2"
    },
    {
        id: 3,
        name: "bot_classe_3"
    },
    {
        id: 4,
        name: "bot_classe_4"
    },
    {
        id: 5,
        name: "bot_classe_5"
    },
    {
        id: 6,
        name: "bot_classe_5"
    },
    {
        id: 7,
        name: "bot_classe_5"
    }
];

module.exports = async(client) => {
    client.user.setPresence({
        game: {
            name: "Nouveau bot - Tuto"
        }
    })
    
    classeTest.forEach(element => {
        let classExist = client.guilds.first().roles.find((r) => r.name === element.name);
        !classExist ? client.guilds.first().createRole({
            name: element.name,
            color: "#" + Math.floor(Math.random()*16777215).toString(16),
            mentionable: true,
        }) : null;
    });
    
};