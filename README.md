# bot_discord

Projet pour le cours d'algo

  Ce bot permet à un élève de rejoindre une classe de pouvoir examiner ses cours et de cocher si un cours a été fait ou pas. 

  Un administrateur peux créer une classe et de créer des cours.

  Les commandes disponibles : 

    !join {nom de la classe}

    !stats {nom de la classe}

    !cours

    !check {nom du cours}

@author : Julien CORNU, Sylvain BOUCHEZ, Quentin HARNAY

# bot_discord