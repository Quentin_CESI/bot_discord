const Discord = require('discord.js');
const client = new Discord.Client();
const fs = require("fs");
const Classes = require('./Controllers/classes.js');
client.login("NjUxNDE4NTA4OTk5MjYyMjQ4.Xeuhdg.CUKGOoEglrXYOS1Bx2EPbG6i9dQ");

//Classes test
const classeTest = [
    {
        id: 1,
        name: "bot_classe_1"
    },
    {
        id: 2,
        name: "bot_classe_2"
    },
    {
        id: 3,
        name: "bot_classe_3"
    }
];

client.commands = new Discord.Collection();
fs.readdir("./Commands/", (error, f) => {
    if(error) console.log(error);
    // filtre des fichiers .js
    let commands = f.filter(f => f.split(".").pop() === "js");
    if (commands.length <= 0) return console.log("Aucune commande trouvée");
    
    commands.forEach((f) => {
        let command = require(`./Commands/${f}`);
        console.log(`${f} commande chargée !`);

        client.commands.set(command.help.name, command);
    });
});

fs.readdir("./Events/", (error, f) => {
    if(error) console.log(error);
    console.log(`${f.length} Évènement${f.length > 1 ? 's' : null} en chargement`);

    f.forEach((files) => {
        const events = require(`./Events/${files}`);
        const event = files.split(".")[0];

        client.on(event, events.bind(null, client));
    });
});


// Faudrais déplacer ce truc dans un event ca ca me plait pas ici...
// Event
client.on("guildMemberAdd", (member) => {
    Classes.getAllClasses().then((value) => {
        let fieldsClass = [];
        value.forEach(element=>{
            fieldsClass.push(
                {
                    name: 'Nom :',
                    value: element.name 
                });
        })
        member.createDM().then(channel => {
            channel.send({
                embed: {
                    color: 0xe43333,
                    title: `Veuillez saisir votre classe`,
                    fields: fieldsClass
                }
            });
        })
        member.guild.channels.find(c => c.name === "général").send(`"${member.user.username}" has joined this server`);
    });
});
